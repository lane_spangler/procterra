// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include <vector>
#include "ProcTerra.h"

#ifndef PERLINNOISEGEN_H
#define PERLINNOISEGEN_H

class PROCTERRA_API PerlinNoiseGen {
	// The permutation vector
	std::vector<int> p;
public:
	// Initialize with the reference values for the permutation vector
	PerlinNoiseGen();
	// Generate a new permutation vector based on the value of seed
	PerlinNoiseGen(unsigned int seed);
	// Get a noise value, for 2D images z can have any value
	double noise(double x, double y, double z);
	float freqX;
	float freqY;
	float freqZ;
	float amplitude;
	float SCALE;
private:
	double fade(double t);
	double lerp(double t, double a, double b);
	double grad(int hash, double x, double y, double z);
};

#endif

