// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

/**
 * 
 */
#ifndef		HAS_OCTREE_H_BEEN_INCLUDED
#define		HAS_OCTREE_H_BEEN_INCLUDED

#include "ProcTerra.h"
#include "qef.h"
#include "mesh.h"
#include "Density.h"
#include "ProceduralMeshComponent.h"
// ----------------------------------------------------------------------------

enum OctreeNodeType
{
	Node_None,
	Node_Internal,
	Node_Psuedo,
	Node_Leaf,
};

// ----------------------------------------------------------------------------

struct OctreeDrawInfo 
{
	OctreeDrawInfo()
		: index(-1)
		, corners(0)
	{
	}
	UPROPERTY()
	int				index;
	UPROPERTY()
	int				corners;
	UPROPERTY()
	FVector			position;
	UPROPERTY()
	FVector			averageNormal;
	UPROPERTY()
	svd::QefData	qef;
};

// ----------------------------------------------------------------------------

class PROCTERRA_API OctreeNode
{
public:

	OctreeNode()
		: type(Node_None)
		, min(0, 0, 0)
		, size(0)
		, drawInfo(nullptr)
	{
		for (int i = 0; i < 8; i++)
		{
			children[i] = nullptr;
		}
	}

	OctreeNode(const OctreeNodeType _type)
		: type(_type)
		, min(0, 0, 0)
		, size(0)
		, drawInfo(nullptr)
	{
		for (int i = 0; i < 8; i++)
		{
			children[i] = nullptr;
		}
	}
	UPROPERTY()
	OctreeNodeType	type;
	UPROPERTY()
	FVector			min;
	UPROPERTY()
	int				size;
	UPROPERTY()
	OctreeNode*		children[8];
	UPROPERTY()
	OctreeDrawInfo*	drawInfo;
};

// ----------------------------------------------------------------------------

OctreeNode* BuildOctree(const FVector& min, const int size, const float threshold);
void DestroyOctree(OctreeNode* node);
void GenerateMeshFromOctree(OctreeNode* node, TArray<MeshVertex>& vertexBuffer, TArray<int32>& indexBuffer, TArray<FProcMeshTangent>& Tangents, TArray<FVector2D>& UVs);

// ----------------------------------------------------------------------------

#endif	// HAS_OCTREE_H_BEEN_INCLUDED

