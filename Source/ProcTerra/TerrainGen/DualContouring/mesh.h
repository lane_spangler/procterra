// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include <vector>


// ----------------------------------------------------------------------------

struct MeshVertex
{
	MeshVertex(const FVector _xyz, const FVector _normal)
	: xyz(_xyz)
	, normal(_normal)
	{
	}

	FVector	xyz, normal;
};
