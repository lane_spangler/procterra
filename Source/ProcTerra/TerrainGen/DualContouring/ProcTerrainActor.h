// A simple procedural cylinder example
// 27. May 2015 - Sigurdur G. Gunnarsson

#pragma once

#include "GameFramework/Actor.h"
#include "ProcTerra.h"
#include "ProceduralMeshComponent.h"
#include "Density.h"
#include "octree.h"
#include "mesh.h"
#include "ProcTerrainActor.generated.h"

UCLASS()
class AProcTerrainActor : public AActor
{
	GENERATED_BODY()

public:
	AProcTerrainActor();
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Terrain Parameters")
		int32 OctreeSize = 64;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Terrain Parameters")
		float Threshold = 5.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Terrain Parameters")
		float xFrequency = 1.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Terrain Parameters")
		float yFrequency = 1.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Terrain Parameters")
		float zFrequency = 1.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Terrain Parameters")
		float Amplitude = 10.0f;
	
	virtual void OnConstruction(const FTransform& Transform) override;
	virtual void BeginPlay() override;
	virtual void BeginDestroy() override;

	OctreeNode* chunkRoot;

private:
	void GenerateMesh();

	UPROPERTY(VisibleAnywhere, Category = Materials)
		UProceduralMeshComponent* mesh;


	void GenerateChunk();
};
