// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "ProcTerra.h"
#include "TerrainGen/PerlinNoiseGen.h"
/**
 * 
 */
	extern PerlinNoiseGen* basePerlin;
	float GetDensity(FVector p);
	int newSeed();
	float Sphere(const FVector& worldPosition, const FVector& origin, float radius);
	float Cuboid(const FVector& worldPosition, const FVector& origin, const FVector& halfDimensions);
	void setAmplitude(float a);
	void setFrequency(float x, float y, float z);
	extern int printCount;



