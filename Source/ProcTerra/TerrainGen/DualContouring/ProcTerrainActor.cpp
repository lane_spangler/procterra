// Fill out your copyright notice in the Description page of Project Settings.

#include "ProcTerra.h"
#include "ProcTerrainActor.h"
#include "ProceduralMeshComponent.h"
#include "octree.h"
#include "mesh.h"
#include "Density.h"

AProcTerrainActor::AProcTerrainActor()
{
	mesh = CreateDefaultSubobject<UProceduralMeshComponent>(TEXT("ProceduralMesh"));
	RootComponent = mesh;
}

void AProcTerrainActor::OnConstruction(const FTransform& Transform)
{
	Super::OnConstruction(Transform);
	GenerateMesh();
}

void AProcTerrainActor::BeginPlay()
{
	Super::BeginPlay();
	GenerateMesh();
}

void AProcTerrainActor::BeginDestroy()
{
	Super::BeginDestroy();
	DestroyOctree(chunkRoot);
	if (mesh)
	{
		UE_LOG(LogTemp, Warning, TEXT("Clearing mesh"));
		mesh->ClearAllMeshSections();
	}
}


void AProcTerrainActor::GenerateMesh()
{
	newSeed();
	setAmplitude(Amplitude);
	setFrequency(xFrequency, yFrequency, zFrequency);

	
	GenerateChunk();

}


void AProcTerrainActor::GenerateChunk()
{
	TArray<MeshVertex> MeshVertices;
	TArray<FVector> Vertices;
	TArray<int32> Triangles;
	TArray<FVector> Normals;
	TArray<FVector2D> UVs;
	TArray<FProcMeshTangent> Tangents;
	TArray<FColor> VertexColors;

	chunkRoot = BuildOctree(GetActorLocation(), OctreeSize, Threshold);
	GenerateMeshFromOctree(chunkRoot, MeshVertices, Triangles,Tangents,UVs);

	for (int i = 0; i < MeshVertices.Num(); i++)
	{
		Vertices.Add(MeshVertices[i].xyz);
		Normals.Add(MeshVertices[i].normal);
	}

	mesh->ClearAllMeshSections();
	UE_LOG(LogTemp, Warning, TEXT("Vertex count: %d"),Vertices.Num());
	mesh->CreateMeshSection(0, Vertices, Triangles, Normals, UVs, VertexColors, Tangents, false);
	/*int unitsPerVert = 100 * tileWidth;
	int NumVerts = width*height*depth;
	Triangles.Reset();

	Vertices.Reset();
	Vertices.AddUninitialized(NumVerts);

	Normals.Reset();
	Normals.AddUninitialized(NumVerts);

	Tangents.Reset();
	Tangents.AddUninitialized(NumVerts);

	UVs.Reset();
	UVs.AddUninitialized(NumVerts);

	//Position Vertices
	int vertIndex = 0;
	const float range = 5000;
	for (int32 x = 0; x < width*unitsPerVert; x+=unitsPerVert)
	{
		for (int32 y = 0; y < height*unitsPerVert; y+=unitsPerVert)
		{
			double vx = (double)x / ((double)(width*unitsPerVert));
			double vy = (double)y / ((double)(height*unitsPerVert));
			FVector v = FVector(x, y, range*noiseGen.noise(10*vx, 10*vy, 0.8));
			
			//UE_LOG(LogTemp, Warning, TEXT("%s"),*v.ToCompactString());
			UVs[vertIndex] = FVector2D(x/unitsPerVert, y/unitsPerVert);

			Vertices[vertIndex] = v;
			vertIndex++;
		}
	}
	vertIndex = 0;
	//Extrapolate Tris and Normals

	for (int32 x = 0; x < width*unitsPerVert-unitsPerVert; x += unitsPerVert)
	{
		for (int32 y = 0; y < height*unitsPerVert-unitsPerVert; y += unitsPerVert)
		{
			int Vert1 = vertIndex;
			int Vert2 = vertIndex + width;
			int Vert3 = vertIndex + width + 1;
			int Vert4 = vertIndex + 1;
			Triangles.Add(Vert4);
			Triangles.Add(Vert2);
			Triangles.Add(Vert1);

			Triangles.Add(Vert3);
			Triangles.Add(Vert2);
			Triangles.Add(Vert4);

			FVector Normal1 = FVector::CrossProduct(Vertices[Vert2] - Vertices[Vert1], Vertices[Vert4] - Vertices[Vert1]).GetSafeNormal();
			FVector Normal2 = FVector::CrossProduct(Vertices[Vert2] - Vertices[Vert4], Vertices[Vert3] - Vertices[Vert4]).GetSafeNormal();
			Normals[Vert1] = Normal1;
			Normals[Vert2] = Normal1;
			Normals[Vert3] = Normal2;
			Normals[Vert4] = Normal2;



			FVector SurfaceTangent = Vertices[Vert1] - Vertices[Vert2];
			FVector SurfaceTangent2 = Vertices[Vert3] - Vertices[Vert2];

			SurfaceTangent = SurfaceTangent.GetSafeNormal();
			Tangents[Vert1] = FProcMeshTangent(SurfaceTangent, true);
			Tangents[Vert2] = FProcMeshTangent(SurfaceTangent, true);
			Tangents[Vert3] = FProcMeshTangent(SurfaceTangent2, true);
			Tangents[Vert4] = FProcMeshTangent(SurfaceTangent2, true);
			vertIndex++;
		}
		vertIndex++;
	}
		*/

}

