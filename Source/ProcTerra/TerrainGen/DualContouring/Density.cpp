// Fill out your copyright notice in the Description page of Project Settings.

#include "ProcTerra.h"
#include "Density.h"
#include "TerrainGen/PerlinNoiseGen.h"

PerlinNoiseGen* basePerlin;
int printCount = 0;
float GetDensity(FVector p)
{
	float vx = p.X*basePerlin->SCALE;
	float vy = p.Y*basePerlin->SCALE;
	//float vz = p.Z*basePerlin->SCALE;
	//float density = basePerlin->noise(vx*basePerlin->freqX, vy*basePerlin->freqY, vz*basePerlin->freqZ)*basePerlin->amplitude-(basePerlin->amplitude/2.0f);
	float terrain = p.Z - basePerlin->noise(vx*basePerlin->freqX, vy*basePerlin->freqY, 0.1f)*basePerlin->amplitude;


	printCount++;
	if (printCount > 32)
	{
		UE_LOG(LogTemp, Warning, TEXT("Original: %s"), *p.ToCompactString());
		UE_LOG(LogTemp, Warning, TEXT("SCALE: %f"), basePerlin->SCALE);
		//UE_LOG(LogTemp, Warning, TEXT("Density: %f"), density);
		//UE_LOG(LogTemp, Warning, TEXT("Height: %f"), terrain);
		printCount = -10000000;
	}
	float sphere = Sphere(p, FVector(45, 45, 10), 15);
	float innersphere = Sphere(p, FVector(45, 45, 10), 10);
	float cube = Cuboid(p, FVector(45, 35, 10), FVector(5, 5, 5));
	return FMath::Max(-cube, FMath::Max(-innersphere,FMath::Min(terrain, sphere)));
}

float Sphere(const FVector& worldPosition, const FVector& origin, float radius)
{
	return (worldPosition - origin).Size() - radius;
}

// ----------------------------------------------------------------------------

float Cuboid(const FVector& worldPosition, const FVector& origin, const FVector& halfDimensions)
{
	const FVector& local_pos = worldPosition - origin;
	const FVector& pos = local_pos;

	const FVector& d = pos.GetAbs() - halfDimensions;
	const float m = FMath::Max(d.X, FMath::Max(d.Y, d.Z));
	return FMath::Min(m, FMath::Max(d.Size(), 0.0f));
}

int newSeed()
{
	delete basePerlin;
	printCount = 0;
	int s = rand();
	basePerlin = new PerlinNoiseGen(s);
	basePerlin->SCALE = (1.0f/64.0f);
	return s;
}

void setAmplitude(float a)
{
	basePerlin->amplitude = a;
}
void setFrequency(float x, float y, float z)
{
	basePerlin->freqX = x;
	basePerlin->freqY = y;
	basePerlin->freqZ = z;
}
