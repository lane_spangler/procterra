// Fill out your copyright notice in the Description page of Project Settings.

#include "ProcTerra.h"
#include "octree.h"
#include "Density.h"

// ----------------------------------------------------------------------------

const int MATERIAL_AIR = 0;
const int MATERIAL_SOLID = 1;

const float QEF_ERROR = 1e-6f;
const int QEF_SWEEPS = 4;

// ----------------------------------------------------------------------------

const FVector CHILD_MIN_OFFSETS[] =
{
	// needs to match the vertMap from Dual Contouring impl
	FVector(0, 0, 0),
	FVector(0, 0, 1),
	FVector(0, 1, 0),
	FVector(0, 1, 1),
	FVector(1, 0, 0),
	FVector(1, 0, 1),
	FVector(1, 1, 0),
	FVector(1, 1, 1),
};

// ----------------------------------------------------------------------------
// data from the original DC impl, drives the contouring process

const int edgevmap[12][2] =
{
	{ 0, 4 }, { 1, 5 }, { 2, 6 }, { 3, 7 },	// x-axis 
	{ 0, 2 }, { 1, 3 }, { 4, 6 }, { 5, 7 },	// y-axis
	{ 0, 1 }, { 2, 3 }, { 4, 5 }, { 6, 7 }		// z-axis
};

const int edgemask[3] = { 5, 3, 6 };

const int vertMap[8][3] =
{
	{ 0, 0, 0 },
	{ 0, 0, 1 },
	{ 0, 1, 0 },
	{ 0, 1, 1 },
	{ 1, 0, 0 },
	{ 1, 0, 1 },
	{ 1, 1, 0 },
	{ 1, 1, 1 }
};

const int faceMap[6][4] = { { 4, 8, 5, 9 }, { 6, 10, 7, 11 }, { 0, 8, 1, 10 }, { 2, 9, 3, 11 }, { 0, 4, 2, 6 }, { 1, 5, 3, 7 } };
const int cellProcFaceMask[12][3] = { { 0, 4, 0 }, { 1, 5, 0 }, { 2, 6, 0 }, { 3, 7, 0 }, { 0, 2, 1 }, { 4, 6, 1 }, { 1, 3, 1 }, { 5, 7, 1 }, { 0, 1, 2 }, { 2, 3, 2 }, { 4, 5, 2 }, { 6, 7, 2 } };
const int cellProcEdgeMask[6][5] = { { 0, 1, 2, 3, 0 }, { 4, 5, 6, 7, 0 }, { 0, 4, 1, 5, 1 }, { 2, 6, 3, 7, 1 }, { 0, 2, 4, 6, 2 }, { 1, 3, 5, 7, 2 } };

const int faceProcFaceMask[3][4][3] = {
	{ { 4, 0, 0 }, { 5, 1, 0 }, { 6, 2, 0 }, { 7, 3, 0 } },
	{ { 2, 0, 1 }, { 6, 4, 1 }, { 3, 1, 1 }, { 7, 5, 1 } },
	{ { 1, 0, 2 }, { 3, 2, 2 }, { 5, 4, 2 }, { 7, 6, 2 } }
};

const int faceProcEdgeMask[3][4][6] = {
	{ { 1, 4, 0, 5, 1, 1 }, { 1, 6, 2, 7, 3, 1 }, { 0, 4, 6, 0, 2, 2 }, { 0, 5, 7, 1, 3, 2 } },
	{ { 0, 2, 3, 0, 1, 0 }, { 0, 6, 7, 4, 5, 0 }, { 1, 2, 0, 6, 4, 2 }, { 1, 3, 1, 7, 5, 2 } },
	{ { 1, 1, 0, 3, 2, 0 }, { 1, 5, 4, 7, 6, 0 }, { 0, 1, 5, 0, 4, 1 }, { 0, 3, 7, 2, 6, 1 } }
};

const int edgeProcEdgeMask[3][2][5] = {
	{ { 3, 2, 1, 0, 0 }, { 7, 6, 5, 4, 0 } },
	{ { 5, 1, 4, 0, 1 }, { 7, 3, 6, 2, 1 } },
	{ { 6, 4, 2, 0, 2 }, { 7, 5, 3, 1, 2 } },
};

const int processEdgeMask[3][4] = { { 3, 2, 1, 0 }, { 7, 5, 6, 4 }, { 11, 10, 9, 8 } };

// -------------------------------------------------------------------------------

OctreeNode* SimplifyOctree(OctreeNode* node, float threshold)
{
	if (!node)
	{
		return NULL;
	}

	if (node->type != Node_Internal)
	{
		// can't simplify!
		return node;
	}

	svd::QefSolver qef;
	int signs[8] = { -1, -1, -1, -1, -1, -1, -1, -1 };
	int midsign = -1;
	int edgeCount = 0;
	bool isCollapsible = true;

	for (int i = 0; i < 8; i++)
	{
		node->children[i] = SimplifyOctree(node->children[i], threshold);
		if (node->children[i])
		{
			OctreeNode* child = node->children[i];
			if (child->type == Node_Internal)
			{
				isCollapsible = false;
			}
			else
			{
				qef.add(child->drawInfo->qef);

				midsign = (child->drawInfo->corners >> (7 - i)) & 1;
				signs[i] = (child->drawInfo->corners >> i) & 1;

				edgeCount++;
			}
		}
	}

	if (!isCollapsible)
	{
		// at least one child is an internal node, can't collapse
		return node;
	}

	svd::Vec3 qefPosition;
	qef.solve(qefPosition, QEF_ERROR, QEF_SWEEPS, QEF_ERROR);
	float error = qef.getError();

	// convert to glm vec3 for ease of use
	FVector position(qefPosition.x, qefPosition.y, qefPosition.z);

	// at this point the masspoint will actually be a sum, so divide to make it the average
	if (error > threshold)
	{
		// this collapse breaches the threshold
		return node;
	}

	if (position.X < node->min.X || position.X >(node->min.X + node->size) ||
		position.Y < node->min.Y || position.Y >(node->min.Y + node->size) ||
		position.Z < node->min.Z || position.Z >(node->min.Z + node->size))
	{
		const auto& mp = qef.getMassPoint();
		position = FVector(mp.x, mp.y, mp.z);
	}

	// change the node from an internal node to a 'psuedo leaf' node
	OctreeDrawInfo* drawInfo = new OctreeDrawInfo;

	for (int i = 0; i < 8; i++)
	{
		if (signs[i] == -1)
		{
			// Undetermined, use centre sign instead
			drawInfo->corners |= (midsign << i);
		}
		else
		{
			drawInfo->corners |= (signs[i] << i);
		}
	}

	drawInfo->averageNormal = FVector(0.f);
	for (int i = 0; i < 8; i++)
	{
		if (node->children[i])
		{
			OctreeNode* child = node->children[i];
			if (child->type == Node_Psuedo ||
				child->type == Node_Leaf)
			{
				drawInfo->averageNormal += child->drawInfo->averageNormal;
			}
		}
	}

	drawInfo->averageNormal.Normalize();
	drawInfo->position = position;
	drawInfo->qef = qef.getData();

	for (int i = 0; i < 8; i++)
	{
		DestroyOctree(node->children[i]);
		node->children[i] = nullptr;
	}

	node->type = Node_Psuedo;
	node->drawInfo = drawInfo;

	return node;
}

// ----------------------------------------------------------------------------

void GenerateVertexIndices(OctreeNode* node, TArray<MeshVertex>& vertexBuffer)
{
	if (!node)
	{
		return;
	}

	if (node->type != Node_Leaf)
	{
		for (int i = 0; i < 8; i++)
		{
			GenerateVertexIndices(node->children[i], vertexBuffer);
		}
	}

	if (node->type != Node_Internal)
	{
		OctreeDrawInfo* d = node->drawInfo;
		if (!d)
		{
			UE_LOG(LogTemp, Warning, TEXT("Error! Could not add vertex!\n"));
			return;
		}

		d->index = vertexBuffer.Num();
		vertexBuffer.Add(MeshVertex(d->position, d->averageNormal));
	}
}

// ----------------------------------------------------------------------------

int quadIndex = 0;
void ContourProcessEdge(OctreeNode* node[4], int dir, TArray<int32>& indexBuffer, TArray<MeshVertex>& vertexBuffer, TArray<FProcMeshTangent>& Tangents, TArray<FVector2D>& UVs)
{
	int minSize = 1000000;		// arbitrary big number
	int minIndex = 0;
	int indices[4] = { -1, -1, -1, -1 };
	bool flip = false;
	bool signChange[4] = { false, false, false, false };

	for (int i = 0; i < 4; i++)
	{
		const int edge = processEdgeMask[dir][i];
		const int c1 = edgevmap[edge][0];
		const int c2 = edgevmap[edge][1];

		const int m1 = (node[i]->drawInfo->corners >> c1) & 1;
		const int m2 = (node[i]->drawInfo->corners >> c2) & 1;

		if (node[i]->size < minSize)
		{
			minSize = node[i]->size;
			minIndex = i;
			flip = m1 != MATERIAL_AIR;
		}

		indices[i] = node[i]->drawInfo->index;

		signChange[i] =
			(m1 == MATERIAL_AIR && m2 != MATERIAL_AIR) ||
			(m1 != MATERIAL_AIR && m2 == MATERIAL_AIR);
	}

	if (signChange[minIndex])
	{
		if (!flip)
		{
			indexBuffer.Add(indices[3]);
			indexBuffer.Add(indices[1]);
			indexBuffer.Add(indices[0]);

			indexBuffer.Add(indices[2]);
			indexBuffer.Add(indices[3]);
			indexBuffer.Add(indices[0]);
		}
		else
		{
			indexBuffer.Add(indices[1]);
			indexBuffer.Add(indices[3]);
			indexBuffer.Add(indices[0]);

			indexBuffer.Add(indices[3]);
			indexBuffer.Add(indices[2]);
			indexBuffer.Add(indices[0]);
		}
		FVector edge = vertexBuffer[indices[1]].xyz - vertexBuffer[indices[3]].xyz;
		edge = edge.GetSafeNormal();
		FProcMeshTangent t(edge, true);
		Tangents[indices[3]] = t; Tangents[indices[1]] = t;

		//edge = vertexBuffer[indices[2]].xyz - vertexBuffer[indices[3]].xyz;
		//edge = edge.GetSafeNormal();
		//t = FProcMeshTangent(edge, true);
		Tangents[indices[2]] = t; Tangents[indices[0]] = t;

		UVs[indices[1]] = FVector2D(quadIndex, quadIndex);
		UVs[indices[3]] = FVector2D(quadIndex+1, quadIndex);
		UVs[indices[0]] = FVector2D(quadIndex, quadIndex+1);
		UVs[indices[2]] = FVector2D(quadIndex + 1, quadIndex + 1);
		quadIndex++;
	}
}

// ----------------------------------------------------------------------------

void ContourEdgeProc(OctreeNode* node[4], int dir, TArray<int32>& indexBuffer, TArray<MeshVertex>& vertexBuffer, TArray<FProcMeshTangent>& Tangents, TArray<FVector2D>& UVs)
{
	if (!node[0] || !node[1] || !node[2] || !node[3])
	{
		return;
	}

	if (node[0]->type != Node_Internal &&
		node[1]->type != Node_Internal &&
		node[2]->type != Node_Internal &&
		node[3]->type != Node_Internal)
	{
		ContourProcessEdge(node, dir, indexBuffer, vertexBuffer, Tangents, UVs);
	}
	else
	{
		for (int i = 0; i < 2; i++)
		{
			OctreeNode* edgeNodes[4];
			const int c[4] =
			{
				edgeProcEdgeMask[dir][i][0],
				edgeProcEdgeMask[dir][i][1],
				edgeProcEdgeMask[dir][i][2],
				edgeProcEdgeMask[dir][i][3],
			};

			for (int j = 0; j < 4; j++)
			{
				if (node[j]->type == Node_Leaf || node[j]->type == Node_Psuedo)
				{
					edgeNodes[j] = node[j];
				}
				else
				{
					edgeNodes[j] = node[j]->children[c[j]];
				}
			}

			ContourEdgeProc(edgeNodes, edgeProcEdgeMask[dir][i][4], indexBuffer, vertexBuffer, Tangents, UVs);
		}
	}
}

// ----------------------------------------------------------------------------

void ContourFaceProc(OctreeNode* node[2], int dir, TArray<int32>& indexBuffer, TArray<MeshVertex>& vertexBuffer, TArray<FProcMeshTangent>& Tangents, TArray<FVector2D>& UVs)
{
	if (!node[0] || !node[1])
	{
		return;
	}

	if (node[0]->type == Node_Internal ||
		node[1]->type == Node_Internal)
	{
		for (int i = 0; i < 4; i++)
		{
			OctreeNode* faceNodes[2];
			const int c[2] =
			{
				faceProcFaceMask[dir][i][0],
				faceProcFaceMask[dir][i][1],
			};

			for (int j = 0; j < 2; j++)
			{
				if (node[j]->type != Node_Internal)
				{
					faceNodes[j] = node[j];
				}
				else
				{
					faceNodes[j] = node[j]->children[c[j]];
				}
			}

			ContourFaceProc(faceNodes, faceProcFaceMask[dir][i][2], indexBuffer, vertexBuffer, Tangents, UVs);
		}

		const int orders[2][4] =
		{
			{ 0, 0, 1, 1 },
			{ 0, 1, 0, 1 },
		};
		for (int i = 0; i < 4; i++)
		{
			OctreeNode* edgeNodes[4];
			const int c[4] =
			{
				faceProcEdgeMask[dir][i][1],
				faceProcEdgeMask[dir][i][2],
				faceProcEdgeMask[dir][i][3],
				faceProcEdgeMask[dir][i][4],
			};

			const int* order = orders[faceProcEdgeMask[dir][i][0]];
			for (int j = 0; j < 4; j++)
			{
				if (node[order[j]]->type == Node_Leaf ||
					node[order[j]]->type == Node_Psuedo)
				{
					edgeNodes[j] = node[order[j]];
				}
				else
				{
					edgeNodes[j] = node[order[j]]->children[c[j]];
				}
			}

			ContourEdgeProc(edgeNodes, faceProcEdgeMask[dir][i][5], indexBuffer, vertexBuffer, Tangents, UVs);
		}
	}
}

// ----------------------------------------------------------------------------

void ContourCellProc(OctreeNode* node, TArray<int32>& indexBuffer, TArray<MeshVertex>& vertexBuffer, TArray<FProcMeshTangent>& Tangents, TArray<FVector2D>& UVs)
{
	if (node == NULL)
	{
		return;
	}

	if (node->type == Node_Internal)
	{
		for (int i = 0; i < 8; i++)
		{
			ContourCellProc(node->children[i], indexBuffer, vertexBuffer, Tangents, UVs);
		}

		for (int i = 0; i < 12; i++)
		{
			OctreeNode* faceNodes[2];
			const int c[2] = { cellProcFaceMask[i][0], cellProcFaceMask[i][1] };

			faceNodes[0] = node->children[c[0]];
			faceNodes[1] = node->children[c[1]];

			ContourFaceProc(faceNodes, cellProcFaceMask[i][2], indexBuffer, vertexBuffer, Tangents, UVs);
		}

		for (int i = 0; i < 6; i++)
		{
			OctreeNode* edgeNodes[4];
			const int c[4] =
			{
				cellProcEdgeMask[i][0],
				cellProcEdgeMask[i][1],
				cellProcEdgeMask[i][2],
				cellProcEdgeMask[i][3],
			};

			for (int j = 0; j < 4; j++)
			{
				edgeNodes[j] = node->children[c[j]];
			}

			ContourEdgeProc(edgeNodes, cellProcEdgeMask[i][4], indexBuffer, vertexBuffer, Tangents, UVs);
		}
	}
}

// ----------------------------------------------------------------------------

FVector ApproximateZeroCrossingPosition(const FVector& p0, const FVector& p1)
{
	// approximate the zero crossing by finding the min value along the edge
	float minValue = 100000.f;
	float t = 0.f;
	float currentT = 0.f;
	const int steps = 8;
	const float increment = 1.f / (float)steps;
	while (currentT <= 1.f)
	{
		const FVector p = p0 + ((p1 - p0) * currentT);
		const float density = FMath::Abs(GetDensity(p));
		if (density < minValue)
		{
			minValue = density;
			t = currentT;
		}

		currentT += increment;
	}

	return p0 + ((p1 - p0) * t);
}

// ----------------------------------------------------------------------------

FVector CalculateSurfaceNormal(const FVector& p)
{
	const float H = 0.001f;
	const float dx = GetDensity(p + FVector(H, 0.f, 0.f)) - GetDensity(p - FVector(H, 0.f, 0.f));
	const float dy = GetDensity(p + FVector(0.f, H, 0.f)) - GetDensity(p - FVector(0.f, H, 0.f));
	const float dz = GetDensity(p + FVector(0.f, 0.f, H)) - GetDensity(p - FVector(0.f, 0.f, H));
	FVector r(dx, dy, dz);
	r.Normalize();
	return r;
}

// ----------------------------------------------------------------------------

OctreeNode* ConstructLeaf(OctreeNode* leaf)
{
	if (!leaf || leaf->size != 1)
	{
		UE_LOG(LogTemp, Warning, TEXT("Null Leaf!"));
		return nullptr;
	}

	int corners = 0;

	for (int i = 0; i < 8; i++)
	{
		const FVector cornerPos = leaf->min + CHILD_MIN_OFFSETS[i];
		const float density = GetDensity(cornerPos);
		const int material = density < 0.f ? MATERIAL_SOLID : MATERIAL_AIR;
		corners |= (material << i);
	}
	//UE_LOG(LogTemp, Warning, TEXT("Density: %d"), sampleDensity);

	if (corners == 0 || corners == 255)
	{
		// voxel is full inside or outside the volume
		delete leaf;
		return nullptr;
	}


	// otherwise the voxel contains the surface, so find the edge intersections
	const int MAX_CROSSINGS = 6;
	int edgeCount = 0;
	FVector averageNormal(0.f);
	svd::QefSolver qef;

	for (int i = 0; i < 12 && edgeCount < MAX_CROSSINGS; i++)
	{
		const int c1 = edgevmap[i][0];
		const int c2 = edgevmap[i][1];

		const int m1 = (corners >> c1) & 1;
		const int m2 = (corners >> c2) & 1;

		if ((m1 == MATERIAL_AIR && m2 == MATERIAL_AIR) ||
			(m1 == MATERIAL_SOLID && m2 == MATERIAL_SOLID))
		{
			// no zero crossing on this edge
			continue;
		}

		const FVector p1 = leaf->min + CHILD_MIN_OFFSETS[c1];
		const FVector p2 = leaf->min + CHILD_MIN_OFFSETS[c2];
		const FVector p = ApproximateZeroCrossingPosition(p1, p2);
		const FVector n = CalculateSurfaceNormal(p);
		qef.add(p.X, p.Y, p.Z, n.X, n.Y, n.Z);

		averageNormal += n;

		edgeCount++;
	}

	svd::Vec3 qefPosition;
	qef.solve(qefPosition, QEF_ERROR, QEF_SWEEPS, QEF_ERROR);

	OctreeDrawInfo* drawInfo = new OctreeDrawInfo;
	drawInfo->position = FVector(qefPosition.x, qefPosition.y, qefPosition.z);
	drawInfo->qef = qef.getData();

	const FVector min = leaf->min;
	const FVector max = leaf->min + leaf->size;
	if (drawInfo->position.X < min.X || drawInfo->position.X > max.X ||
		drawInfo->position.Y < min.Y || drawInfo->position.Y > max.Y ||
		drawInfo->position.Z < min.Z || drawInfo->position.Z > max.Z)
	{
		const auto& mp = qef.getMassPoint();
		drawInfo->position = FVector(mp.x, mp.y, mp.z);
	}
	drawInfo->averageNormal = averageNormal / (float)edgeCount;
	drawInfo->averageNormal.Normalize();
	drawInfo->corners = corners;

	leaf->type = Node_Leaf;
	leaf->drawInfo = drawInfo;
	return leaf;
}

// -------------------------------------------------------------------------------

OctreeNode* ConstructOctreeNodes(OctreeNode* node)
{
	if (!node)
	{
		return nullptr;
	}

	if (node->size == 1)
	{
		return ConstructLeaf(node);
	}

	const int childSize = node->size / 2;
	bool hasChildren = false;

	for (int i = 0; i < 8; i++)
	{
		OctreeNode* child = new OctreeNode;
		child->size = childSize;
		child->min = node->min + (CHILD_MIN_OFFSETS[i] * childSize);
		child->type = Node_Internal;

		node->children[i] = ConstructOctreeNodes(child);
		hasChildren |= (node->children[i] != nullptr);
	}

	if (!hasChildren)
	{
		delete node;
		return nullptr;
	}

	return node;
}

// -------------------------------------------------------------------------------

OctreeNode* BuildOctree(const FVector& min, const int size, const float threshold)
{
	OctreeNode* root = new OctreeNode;
	root->min = min;
	root->size = size;
	root->type = Node_Internal;

	ConstructOctreeNodes(root);
	root = SimplifyOctree(root, threshold);

	return root;
}

// ----------------------------------------------------------------------------

void GenerateMeshFromOctree(OctreeNode* node, TArray<MeshVertex>& vertexBuffer, TArray<int32>& indexBuffer, TArray<FProcMeshTangent>& Tangents, TArray<FVector2D>& UVs)
{
	if (!node)
	{
		return;
	}

	vertexBuffer.Reset();
	indexBuffer.Reset();
	Tangents.Reset();
	UVs.Reset();
	GenerateVertexIndices(node, vertexBuffer);
	Tangents.AddUninitialized(vertexBuffer.Num());
	UVs.AddUninitialized(vertexBuffer.Num());
	quadIndex = 0;
	ContourCellProc(node, indexBuffer, vertexBuffer, Tangents, UVs);
}

// -------------------------------------------------------------------------------

void DestroyOctree(OctreeNode* node)
{
	if (!node)
	{
		return;
	}

	for (int i = 0; i < 8; i++)
	{
		DestroyOctree(node->children[i]);
	}

	if (node->drawInfo)
	{
		delete node->drawInfo;
	}

	delete node;
}

